fetch('../json.json')
  .then(response => response.json())
  .then(data => {
    let data = [
      { IP: '192.168.0.1', nombre: 'Harold', apellidos: 'Maldonado Garcia', nota: 9.1 },
      { IP: '192.168.0.2', nombre: 'Ana', apellidos: 'García Cruz', nota: 9.0 },
      { IP: '192.168.0.3', nombre: 'Jose', apellidos: 'López Mamani', nota: 7.5 }
    ];
    
    let table = document.querySelector('#tabla');
    
    // Crea una fila para los encabezados de la tabla
    let headerRow = table.insertRow();
    let header1 = headerRow.insertCell();
    let header2 = headerRow.insertCell();
    let header3 = headerRow.insertCell();
    let header4 = headerRow.insertCell();
    header1.innerHTML = "IP";
    header2.innerHTML = "Nombre";
    header3.innerHTML = "Apellidos";
    header4.innerHTML = "Nota";
    
    // Crea una fila para cada elemento del objeto data
    data.forEach(item => {
      let row = table.insertRow();
      let cell1 = row.insertCell();
      let cell2 = row.insertCell();
      let cell3 = row.insertCell();
      let cell4 = row.insertCell();
      cell1.innerHTML = item.IP;
      cell2.innerHTML = item.nombre;
      cell3.innerHTML = item.apellidos;
      cell4.innerHTML = item.nota;
    });
  });

  const juego = document.getElementById('miCanvas');

  const canvas = new fabric.Canvas('miCanvas');


  const imagen = new fabric.Image.fromURL('../img/pacman.png', function(img) {
    
    canvas.add(img);
  });
  canvas.renderAll();





  
